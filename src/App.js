import React, { Component } from "react";
import RepoList from "./components/RepoList";

import "./App.css";

class App extends Component {

    constructor(){
        super();
        this.state = {
            searchResult: "Нет информации",
            searchRepos: []
        };
    }

    onInputKeyDown = (event) => {
        if(event.keyCode === 13)
        {
            if(event.target.value != "")
            {
                this.getUserReposFromGithub(event.target.value);
            }
            else
            {
                alert("После с ником не может быть пустым");
            }
        }
    }

    getUserReposFromGithub = (searchValue) => {
        fetch(`https://api.github.com/users/${searchValue}/repos`)
        .then(response => response.json())
        .then(response => {
            if(response.message === "Not Found")
            {
                this.setState({
                    searchResult: "Пользователь не найден",
                    searchRepos: []
                })
            }
            else
            {
                const repoAmount = response.length;
                const strTemplate = (repoAmount > 0) ? `Найдено ${repoAmount} репо:` : "У пользователя нет репозиториев";
                this.setState({
                    searchResult: strTemplate,
                    searchRepos: response
                })
            }
        })
    }

    render() {
        return(
            <div className="app">
                <div>
                    <h2>Поиск репо на github:</h2>
                    <input className="form-control" placeholder="Ник пользователя" type="text" onKeyDown={this.onInputKeyDown}/>
                </div>
                <hr/>
                <div>
                    <h3>Результат поиска:</h3>
                    <div>{this.state.searchResult}</div>
                    { (this.state.searchRepos.length > 0) ? <RepoList repos={this.state.searchRepos}/> : null }
                </div>
            </div>
        )
    }
}
export default App;