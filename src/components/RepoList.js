import React from "react";
import Repo from "./Repo";

const RepoList = ({repos}) => {
    return (
        <div className="list-group">
           {repos.map(repo => <Repo key={repo.id} repoData={repo}/>)}
        </div>
    );
}

export default RepoList;