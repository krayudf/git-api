import React from "react";

const Repo = ({repoData}) => {
    return(
        <a className="list-group-item list-group-item-action" target="_blank" href={repoData.html_url}>{repoData.name}</a>
    )
}

export default Repo;  